def init_grid(width, height):
    grid = list()
    for i in range(height):
        grid.append(list())
    for lines in grid:
        for j in range(width):
            lines.append('.')
    return grid;


def print_grid(grid):
    for lines in grid:
        for cell in range(len(lines) * 2 + 1):
            print('_', end='') # Print the lines between cells.
        print()
        for cell in range(len(lines) * 2):
            if cell is 0:
                print('|', end='')
            if cell % 2 is 1:
                print('|', end='')
            else:
                print(lines[cell//2], end='')
        print()
    for cell in range(len(lines) * 2 + 1):
        print('_', end='')  # Print the line at the end.
    print()


def choose_column():
    c = int(input('Choose a column:\n 0 1 2 3 4 5\n'))
    return c


def main():
    grid = init_grid(7, 6)
    grid[5][3] = '*'
    win = False
    while not win:
        print_grid(grid)
        choice = choose_column()

main()